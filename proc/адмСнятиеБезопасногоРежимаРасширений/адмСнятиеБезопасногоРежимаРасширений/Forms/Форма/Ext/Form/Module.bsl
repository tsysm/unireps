﻿
&НаСервере
Процедура ПриОткрытииНаСервере()
	ЗащитаОтОпасныхДействий	= новый ОписаниеЗащитыОтОпасныхДействий;
	ЗащитаОтОпасныхДействий.ПредупреждатьОбОпасныхДействиях	=Ложь;
	Расширения	= РасширенияКонфигурации.Получить();
	Для каждого обРасширение Из Расширения Цикл
		Если обРасширение.БезопасныйРежим Тогда
			обРасширение.БезопасныйРежим = Ложь;
			обРасширение.ЗащитаОтОпасныхДействий = ЗащитаОтОпасныхДействий;
		КонецЕсли;
		Попытка
			обРасширение.Записать();
		Исключение
			Сообщить("При снятии безопасного режима для расширения """ + обРасширение.Имя + """ появились ошибки: """ + ОписаниеОшибки() + """");
		КонецПопытки;
	КонецЦикла;
КонецПроцедуры

&НаКлиенте
Процедура ПриОткрытии(Отказ)
	ПриОткрытииНаСервере();
	ЗавершитьРаботуСистемы(Ложь);
КонецПроцедуры
