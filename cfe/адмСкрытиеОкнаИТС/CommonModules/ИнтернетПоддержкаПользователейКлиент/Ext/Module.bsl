﻿&Вместо("ПередНачаломРаботыСистемы")
// Реализует обработку события ПередНачаломРаботыСистемы() клиентского
// приложения. Необходимо реализовать вызов метода из
// МодульУправляемогоПриложения.ПередНачаломРаботыСистемы()
// и МодульОбычногоПриложения.ПередНачаломРаботыСистемы().
//
// Обработчик, вызываемый перед началом работы системы.
//
Процедура адмПередНачаломРаботыСистемы() Экспорт

	ПараметрыПередНачаломРаботы =
		ИнтернетПоддержкаПользователейВызовСервера.ПередНачаломРаботыСистемы(ПараметрыКлиента());
	
	//// Подключение обработчика запроса настроек клиента лицензирования
	//Если ПараметрыПередНачаломРаботы.ДоступнаРаботаСНастройкамиКлиентаЛицензирования Тогда
	//	Попытка
	//		КлиентЛицензированияКлиент.ПодключитьОбработчикБИПДляЗапросаНастроекКлиентаЛицензирования();
	//	Исключение
	//		ИнтернетПоддержкаПользователейВызовСервера.ЗаписатьОшибкуВЖурналРегистрации(
	//			СтроковыеФункцииКлиентСервер.ПодставитьПараметрыВСтроку(
	//				НСтр("ru = 'Не удалось подключить обработчик запроса настроек клиента лицензирования.
	//					|%1'"),
	//				ПодробноеПредставлениеОшибки(ИнформацияОбОшибке())));
	//	КонецПопытки;
	//КонецЕсли;
	//// Конец Подключение обработчика запроса настроек клиента лицензирования

КонецПроцедуры
